Functions for odometers which has the following restriction:

1. The odometer readings will have no ZEROs.

2. The readings of the odometer will always be in strictly ASCENDING order.

3. The reading will maintain a strict size.



Function created are:

1. next\_reading(current\_reading : str) -> str:
2. prev\_reading(current\_reading : str) -> str:
3. skip\_reading(current\_reading : str, step : int) -> str:
4. reverse\_reading(currecurrent\_reading : str, step : int) -> str:
5. step\_between(reading1 : str, reading2 : str) -> int:
